import { getField, updateField } from 'vuex-map-fields'
import countries from '@/plugins/countries.js'

const main = {
    namespaced: true,
    state: () => {
        return {
            createForm: {
                firstName: '',
                lastName: '',
                email: '',
                password: '',
                phoneNumber: '',
                country: countries[217],
                countryId: countries[217].id,
                driverCardImg: null,
                idCardImg: null,
                expDriverCard: null,
                expIdCard: null
            }
        }
    },
    getters: {
        getField
    },
    mutations: {
        updateField,
        setLoadingWrapper(state, payload) {
            state.isAppLoading = payload
        },
        setDrawerMenu(state, payload) {
            state.isShowDrawerMenu = payload
        },
        setFullWrapper(state, payload) {
            state.isShowFullWrapper = payload
        }
    },
    actions: {
        SHOW_APP_LOADING({ commit }, isShow) {
            commit('setLoadingWrapper', isShow)
        },
        TOGGLE_DRAWER_MENU({ commit, state }) {
            commit('setDrawerMenu', !state.isShowDrawerMenu)
            commit('setFullWrapper', !state.isShowFullWrapper)
        },
        SHOW_FULL_WARPPER({ commit }, status) {
            commit('setFullWrapper', status)
        },
        CLOSE_ALL_MENU({ commit }) {
            commit('setDrawerMenu', false)
            commit('setFullWrapper', false)
        }
    }
}

export default main
