import Vue from 'vue'
import Vuex from 'vuex'
import main from './main'

Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        main
    },
    strict: false,
    plugins: []
})

export const strict = false
