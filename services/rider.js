import { httpClient } from './httpClient'

export default {
    createRider: async (data) => {
        const url = '/riders'
        try {
            const res = await httpClient.post(url, data)
            return res.data
        } catch (error) {
            return error
        }
    },
    editRider: async (data) => {
        const url = '/riders/edit'
        try {
            const res = await httpClient.post(url, data)
            return res.data
        } catch (error) {
            return error
        }
    },
    getRider: async (id) => {
        const url = '/'
        try {
            const res = await httpClient.get(url)
            // return res.data
            console.log(res)
            return {
                titleName: 'นาย',
                firstName: 'Elon',
                lastName: 'Musk',
                email: 'panupong.kiangmana@gmail.com',
                phoneNumber: '0875435702',
                country: { id: 218, code: '+66', name: 'Thailand', alpha: 'TH', flagAlpha: 'TH', currency: 'THB' },
                birthday: '07/05/1992',
                idCardNumber: '1571100000000',
                laserCode: 'JT0012345678'
            }
        } catch (error) {
            return error
        }
    },
    // VEHICLE PAGE
    getVehicle: async (riderId) => {
        const url = '/'
        try {
            const res = await httpClient.post(url, riderId)
            // return res.data
            console.log(res)
            return {
                data: {
                    data: {
                        error: {}
                    }
                }
            }
        } catch (error) {
            return error
        }
    },
    deleteVehicle: async (riderId, vehicleId) => {
        const url = '/'
        try {
            const res = await httpClient.post(url, {
                riderId,
                vehicleId
            })
            // return res.data
            console.log(res)
            return {
                data: {
                    data: {
                        error: {}
                    }
                }
            }
        } catch (error) {
            return error
        }
    }
}
