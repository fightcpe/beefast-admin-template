import axios from 'axios'
import join from 'url-join'

axios.interceptors.request.use(async (config)=> {

  const oauthToken = '' //get token from store here
  if (oauthToken != null) {
    let autorizeValue = `Bearer ${oauthToken}`;
    config.headers = { 'Authorization': autorizeValue }
  }
  //config.url = join('http://mobile.beefastdev.com/api/v1', config.url);
  config.url = join('http://127.0.0.1:5202', config.url);
  return config;
  
});
export const httpClient = axios