# build stage
FROM node:12.21.0-alpine

ARG buildenv=dev

WORKDIR /app
COPY package*.json ./

RUN apk update && apk upgrade
RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python git

# COPY .env ./
RUN npm i npm@latest -g
RUN npm install

COPY . .
# RUN npm run build
RUN npm run build

EXPOSE 3000
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD ["npm", "run", build-$buildenv]
CMD ["npm", "run", "start"]
