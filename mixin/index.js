import moment from 'moment-timezone'

const mixin = {
    methods: {
        dateFormat(date, format = 'DD/MM/YYYY') {
            return moment(date).format(format)
        },
        dateTimeFormat(date, format = 'DD/MM/YYYY HH:mm') {
            return moment(date).format(format)
        },
        formatTimeForApi(t) {
            const result = moment(t).utc().format('YYYY-MM-DDTHH:mm:ss') + 'Z'
            return result
        },
        ISODateString(d) {
            function pad(n) { return n < 10 ? '0' + n : n }
            return d.getUTCFullYear() + '-' + pad(d.getUTCMonth() + 1) + '-' + pad(d.getUTCDate()) + 'T' + pad(d.getUTCHours()) + ':' + pad(d.getUTCMinutes()) + ':' + pad(d.getUTCSeconds()) + 'Z'
        },
        numCommas(num) {
            const number = num * 1 == num ? num : 0
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        },
        opDateRangeFormat(dateArr) {
            if (moment(dateArr[0]).format('DD/MM/YYYY') === moment(dateArr[1]).format('DD/MM/YYYY')) {
                return moment(dateArr[0]).format('DD/MM/YYYY')
            } else {
                return `${moment(dateArr[0]).format('DD/MM/YYYY')} - ${moment(dateArr[1]).format('DD/MM/YYYY')}`
            }
        },
        opDateTimeFormat(date) {
            return moment(date).format('DD/MM/YYYY HH:mm')
        },
        opDateFormat(date) {
           return moment(date).format('DD/MM/YYYY')
        },
        isNumber(evt) {
            const event = !evt ? window.event : evt
            const charCode = (event.which) ? event.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return event.preventDefault()
            }
            return true
        },
        timeDateRemain(sqlDateString) {
            const expireDay = moment(sqlDateString).format('YYYY-MM-DD HH:MM')
            const expire = moment(expireDay, 'YYYY-MM-DD HH:MM')
            const now = moment().format('YYYY-MM-DD HH:MM')
            const diff = expire.diff(now)
            const diffDuration = moment.duration(diff)
            return {
                month: diffDuration.months(),
                day: diffDuration.days(),
                hour: diffDuration.hours()
            }
        },
        calculateAge(val) {
            let years = 0
            if (val) {
                const firstDate = moment(val).format('YYYY-MM-DD')
                const secondDate = moment(new Date(), 'YYYY-MM-DD')
                years = secondDate.diff(firstDate, 'year')
            }
            return years
        },
        pointToStampText(text, setting) {
            if (setting == 2) {
                text = text.replace(/พอยท์/g, 'แสตมป์')
                text = text.replace(/Point/g, 'Stamp')
                text = text.replace(/point/g, 'stamp')
            }
            return text
        },
        POS(setting) {
            let text = 'point'
            if (setting == 2) {
                text = text.replace(/พอยท์/g, 'แสตมป์')
                text = text.replace(/Point/g, 'Stamp')
                text = text.replace(/point/g, 'stamp')
            }
            return text
        },
        convertPackageIdToName(typePackageId) {
            const packageName = ['Trial', 'Small', 'Regular', 'Premium', 'Deluxe']
            const name = packageName[typePackageId <= 4 ? (typePackageId - 1) : 4]
            return name
        },
        yearsNotDefault() {
            const currentYear = parseInt(moment().format('YYYY'))
            const yearList = []
            for (let i = currentYear; i >= 2018; i--) {
                yearList.push({
                    title: i,
                    value: i
                })
            }
            return yearList
        },
        resizeImg(e, onProcessedCallback) {
            const elemFile = e.target.files
            const fileName = elemFile[0].name
            const arrName = fileName.split('.')
            const fileType = arrName[arrName.length - 1].toLowerCase()
            const imgAllowType = ['png', 'jpg', 'jpeg']
            let result = {}
            if (imgAllowType.includes(fileType) >= 0) {
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    const file = elemFile[0]
                    if (file) {
                        const reader = new FileReader()
                        reader.onload = function (e) {
                            const img = document.createElement('img')
                            img.src = e.target.result
                            const canvas = document.createElement('canvas')
                            let ctx = canvas.getContext('2d')
                            ctx.drawImage(img, 0, 0)
                            const MAX_WIDTH = 400
                            const MAX_HEIGHT = 400
                            let width = img.width
                            let height = img.height
                            if (width > height) {
                                if (width > MAX_WIDTH) {
                                    height *= MAX_WIDTH / width
                                    width = MAX_WIDTH
                                }
                            } else if (height > MAX_HEIGHT) {
                                width *= MAX_HEIGHT / height
                                height = MAX_HEIGHT
                            }
                            canvas.width = width
                            canvas.height = height
                            ctx = canvas.getContext('2d')
                            ctx.drawImage(img, 0, 0, width, height)
                            const dataurl = canvas.toDataURL(file.type)
                            // convert dataurl to file image
                            const arr = dataurl.split(',')
                            const mime = arr[0].match(/:(.*?);/)[1]
                            const bstr = atob(arr[1])
                            const u8arr = new Uint8Array(bstr.length)
                            let n = bstr.length
                            while (n--) {
                                u8arr[n] = bstr.charCodeAt(n)
                            }
                            const fileResult = new File([u8arr], 'img' + new Date(), { type: mime })
                            result = {
                                dataurl,
                                fileResult
                            }
                            onProcessedCallback({
                                dataurl,
                                fileResult
                            })
                        }
                        reader.readAsDataURL(file)
                        return result
                    }
                } else {
                    alert('The File APIs are not fully supported in this browser.')
                    return false
                }
            } else {
                document.getElementById(e.target.id).value = ''
                this.$swal({
                    title: 'File Type Not Support',
                    text: 'Please upload image type .png .jpg or .jpeg only.',
                    icon: 'error'
                })
            }
        },
        isURLFormat(text) {
            return /^[a-zA-Z0-9-]*$/.test(text)
        },
        isUsername(evt) {
            return /^[a-zA-Z0-9]*$/.test(evt.key) ? true : event.preventDefault()
        },
        isURL(evt) {
            const test = /^[a-zA-Z0-9_-]/.test(evt.key)
            if (!test) {
                return evt.preventDefault()
            }
            return true
        }
    }
}

export default mixin
