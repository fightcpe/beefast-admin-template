const months = [
    {
        title: 'allMonth',
        keyword: 'all', // keyword for change language
        value: 0
    },
    {
        title: 'มกราคม',
        keyword: 'jan',
        value: 1
    },
    {
        title: 'กุมภาพันธ์',
        keyword: 'feb',
        value: 2
    },
    {
        title: 'มีนาคม',
        keyword: 'mar',
        value: 3
    },
    {
        title: 'เมษายน',
        keyword: 'apr',
        value: 4
    },
    {
        title: 'พฤษภาคม',
        keyword: 'may',
        value: 5
    },
    {
        title: 'มิถุนายน',
        keyword: 'jun',
        value: 6
    },
    {
        title: 'กรกฎาคม',
        keyword: 'jul',
        value: 7
    },
    {
        title: 'สิงหาคม',
        keyword: 'aug',
        value: 8
    },
    {
        title: 'กันยายน',
        keyword: 'sep',
        value: 9
    },
    {
        title: 'ตุลาคม',
        keyword: 'oct',
        value: 10
    },
    {
        title: 'พฤศจิกายน',
        keyword: 'nov',
        value: 11
    },
    {
        title: 'ธันวาคม',
        keyword: 'dec',
        value: 12
    }
]

export default months
