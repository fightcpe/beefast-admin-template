import axios from 'axios'
export const BASEPATH = process.env.apiUrl

export const apiApp = axios.create({
    baseURL: BASEPATH
})
