import Vue from 'vue'
import CountryFlag from 'vue-country-flag'

Vue.component('v-country-flag', CountryFlag)
