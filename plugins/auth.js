export default ({ store, redirect, route }) => {
    const loginBypass = true;
    if (!loginBypass) {
        redirect('http://identity.beefastdev.com/account/login')
    }
}
