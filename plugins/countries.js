const callingCountries = require('country-data').callingCountries.all
const flagDebug = {
    ac: 'ai',
    aq: 'ag',
    bq: 'ba',
    cc: 'tc',
    cx: 'tc',
    fx: 'fr',
    gf: 'fr',
    io: 'gb'
}
const countries = callingCountries.map((el, index) => {
    return {
        id: index + 1,
        code: el.countryCallingCodes[0],
        name: el.name,
        alpha: el.alpha2,
        flagAlpha: flagDebug[el.alpha2.toLowerCase()] != undefined ? flagDebug[el.alpha2.toLowerCase()] : el.alpha2,
        currency: el.currencies ? el.currencies[0] : ''
    }
})
export default countries
