module.exports = {
  root: true,
  env: {
      browser: true,
      node: true,
      jquery: true
  },
  parserOptions: {
      parser: 'babel-eslint'
  },
  extends: [
      '@nuxtjs',
      'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
      'vue/html-indent': ['error', 4],
      'space-before-function-paren': 'off',
      'indent': 'off',
      'vue/attributes-order': 'off',
      'vue/html-self-closing': 'off',
      'vue/html-closing-bracket-spacing': 'off',
      'vue/require-prop-types': 'off',
      'eqeqeq': 'off',
      'dot-notation': 'off',
      "arrow-parens": 'off',
      'vue/attribute-hyphenation': 'off',
      "no-console": 'off',
      'no-mixed-operators': 'off',
      "vue/max-attributes-per-line": ["error", {
          "singleline": 7,
          "multiline": {
              "max": 1,
              "allowFirstLine": true
          }
      }]
  }
}
